$(document).ready(function(){

  /* ------------------------------------------- RESTAURANT TABS */

  $('.menu-button').click(function(){
    $('#menu-box').css('display', 'block');
    $('#info-box').css('display', 'none');
    $('#reviews-box').css('display', 'none');
  });
  $('.info-button').click(function(){
    $('#menu-box').css('display', 'none');
    $('#info-box').css('display', 'block');
    $('#reviews-box').css('display', 'none');
  });
  $('.reviews-button').click(function(){
    $('#menu-box').css('display', 'none');
    $('#info-box').css('display', 'none');
    $('#reviews-box').css('display', 'block');
  });

  /* ------------------------------------------- USER MENU */

  $('.user').hover(function(){
    $('.user-options').css('height', '126');
    },function(){
    $('.user-options').css('height', '58');
  });
  $('.submenu').hover(function(){
    $('.user-options').css('height', '126');
    },function(){
    $('.user-options').css('height', '58');
  });

  /* ------------------------------------------- RESPONSIVE MENU */

    function responsiveMenuDisplayOn() {
        $('.rightMenu').css('z-index', '10000');
        $('.rightMenu').css('right', '0');
        $('.focus').css('opacity', '.8');
        $('#contentRightMenu').css('right', '0');
        $('body').css('overflow-y', 'hidden');
    };
    function responsiveMenuDisplayOff() {
        $('.rightMenu').css('right', '-225px');
        $('#contentRightMenu').css('right', '-225px');
        $('.focus').css('opacity', '0');
        $('body').css('overflow-y', 'inherit');
        setTimeout(function() { $('.rightMenu').css('z-index', '-1'); }, 500);
    };
    $('#rightMenuIcon').click(function(){ responsiveMenuDisplayOn(); });
    $('.focus').click(function(){ responsiveMenuDisplayOff(); });

  /* ------------------------------------------- REPEAT ORDER */

  var repeatorder_height = $(".repeat-order").height();
  $('.repeat-order').css('height', repeatorder_height);

  $('.close-icon').click(function(){
    $('.repeat-order').css('opacity', '0');
    $('.repeat-order').css('height', '0');
    $('.repeat-order').css('padding', '0');
    $('.repeat-order').css('margin', '0');
  });
  $('#repeat-order-cta').click(function(){
        goUp();
        $('#repeat-order').css('z-index', '99999');
        $('#repeat-order>div').css('top', '50px');
        $('#repeat-order>.focus').css('opacity', '1');
  });

  /* ------------------------------------------- POPUPS */

  function goUp() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
  }

  $('.showOrder').click(function(){
        goUp();
        $('#order').css('z-index', '99999');
        $('#order>div').css('top', '50px');
        $('#order>.focus').css('opacity', '1');
  });

  $('.login-cta').click(function(){
        goUp();
        $('#login-popup').css('z-index', '99999');
        $('#login-popup>div').css('top', '50px');
        $('#login-popup>.focus').css('opacity', '1');
  });
  $('.register-cta').click(function(){
        goUp();
        $('#register-popup').css('z-index', '99999');
        $('#register-popup>div').css('top', '50px');
        $('#register-popup>.focus').css('opacity', '1');
  });

  $('#cuisines-link').click(function(){
        goUp();
        $('#cuisins-popup').css('z-index', '99999');
        $('#cuisins-popup>div').css('top', '50px');
        $('#cuisins-popup>.focus').css('opacity', '1');
  });

  $('#diets-link').click(function(){
        goUp();
        $('#diets-popup').css('z-index', '99999');
        $('#diets-popup>div').css('top', '50px');
        $('#diets-popup>.focus').css('opacity', '1');
  });

  $('#meals-link').click(function(){
        goUp();
        $('#meals-popup').css('z-index', '99999');
        $('#meals-popup>div').css('top', '50px');
        $('#meals-popup>.focus').css('opacity', '1');
  });

  $('.hidePopup').click(function(){
      $('.popup>div').css('top', '-2000px');
      $('.focus').css('opacity', '0');
      setTimeout(function() { $('.popup').css('z-index', '-2'); }, 500);
  });

  $('.hidePopupResponsive').click(function(){
      $('#login-popup>div').css('top', '-2000px');
      $('#register-popup>div').css('top', '-2000px');
      $('#login-popup>.focus').css('opacity', '0');
      $('#register-popup>.focus').css('opacity', '0');
      setTimeout(function() { $('#login-popup').css('z-index', '-2'); }, 500);
      setTimeout(function() { $('#register-popup').css('z-index', '-2'); }, 500);
  });

});
